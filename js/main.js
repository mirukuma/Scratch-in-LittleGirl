function draw(JSONData){
  var parsedJSON = JSON.parse(JSONData);

  scripts = parsedJSON.children[0].scripts;
  for (var i = 0; i < scripts.length; i++){
    console.log(scripts[i])
    drawScript(scripts[i][2],scripts[i][0],scripts[i][1]);
  }
}

function drawScript(script,xInput,yInput){
  var x = xInput
  var y = yInput
  var yChange = 0;
  for (var i = 0; i < script.length; i++){
    yChange += drawBlock(script[i],x,y+yChange)
  }
  return yChange

}

function drawBlock(block,x,y){
  if(block[0] == "whenGreenFlag"){
    ctx.fillStyle = "#C6822F";
    ctx.strokeStyle = "#B07020";
    ctx.fillRect(x,y+10,9 * block[0].length,20)
    ctx.strokeRect(x,y+10,9 * block[0].length,20)

    ctx.beginPath()
    ctx.arc(x+20,y+31,Math.sqrt(2)*20,-Math.PI * 3 / 4,-Math.PI/4)
    ctx.fill()
    ctx.fillStyle = "#F0F0F0";
    ctx.font = "15px Arial"
    ctx.fillText(block[0],x,y+25)
    return 30
  }
  else if(block[0] == "doForever"){
      ctx.fillStyle = "#E0A81A";
      ctx.strokeStyle = "#D0A010";

      console.log(block[1])
      yChange = drawScript(block[1],x+23,y+21)
      ctx.fillStyle = "#E0A81A";
      ctx.strokeStyle = "#D0A010";
      ctx.strokeRect(x-1,y-1,9 * block[0].length+2,20+1)
      ctx.strokeRect(x-1,y-1,20+2,yChange + 20+2)
      ctx.strokeRect(x-1,y+1 + yChange + 20,9 * block[0].length+2, 20+1)
      ctx.fillRect(x,y,9 * block[0].length,20)
      ctx.fillRect(x,y,20,yChange + 20)
      ctx.fillRect(x,y+1 + yChange + 20,9 * block[0].length, 20)


      ctx.fillStyle = "#F0F0F0";
      ctx.font = "15px Arial"
      ctx.fillText(block[0],x,y+15)
      console.log(yChange)
      return yChange + 40
  }
  else{
    ctx.fillStyle = "#4A6CD3";
    ctx.strokeStyle = "#3050C0";
    ctx.fillRect(x,y,9 * block[0].length,20)
    ctx.strokeRect(x,y,9 * block[0].length,20)
    ctx.fillStyle = "#F0F0F0";
    ctx.font = "15px Arial"
    ctx.fillText(block[0],x,y+15)
    console.log(block[0])
    return 20
  }
}
